﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DuaTaxi.DBContext.Migrations
{
    public partial class initial2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "c8671752-d023-425a-b8c1-df5850079100");

            migrationBuilder.AddColumn<string>(
                name: "CarPlate",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "c2ebb52a-9e24-48ec-aa4f-ff9739c0ae1c", "a5ae243e-988b-4f03-b906-fcad44b5eaaa", "consumer", "CONSUMER" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "c2ebb52a-9e24-48ec-aa4f-ff9739c0ae1c");

            migrationBuilder.DropColumn(
                name: "CarPlate",
                table: "AspNetUsers");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "c8671752-d023-425a-b8c1-df5850079100", "90260d33-0699-478a-b3c1-3741aed978fe", "consumer", "CONSUMER" });
        }
    }
}
