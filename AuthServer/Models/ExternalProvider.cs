﻿

using System.ComponentModel.DataAnnotations;

namespace AuthServer.Models
{
    public class ExternalProvider
    {
        public string DisplayName { get; set; }
        public string AuthenticationScheme { get; set; }
    }

    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginModel
    {
        public string Provider { get; set; }
        public string ReturnUrl { get; set; }
    }

}
