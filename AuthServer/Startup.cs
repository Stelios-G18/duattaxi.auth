﻿using AuthServer.Extensions;
using AuthServer.Infrastructure.Data.Identity;
using AuthServer.Infrastructure.Services;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Consul;
using DuaTaxi.AuthServer.Messages.DeleteAccount;
using DuaTaxi.AuthServer.Messages.UpdatePassword;
using DuaTaxi.AuthServer.Messages.UpdateUserInfo;
using DuaTaxi.Common;
using DuaTaxi.Common.Authentication;
using DuaTaxi.Common.Consul;
using DuaTaxi.Common.CustomApiCheck;
using DuaTaxi.Common.Dispatchers;
using DuaTaxi.Common.Jaeger;
using DuaTaxi.Common.Mvc;
using DuaTaxi.Common.RabbitMq;
using DuaTaxi.Common.Swagger;
using DuaTaxi.DBContext.Data.Context;
using DuaTaxi.Entities.Core.Models;
using IdentityServer4;
using IdentityServer4.AccessTokenValidation;
using IdentityServer4.AspNetIdentity;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using System;
using System.Net;
using System.Reflection;
using System.Security.Claims;

namespace AuthServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        private static readonly string[] Headers = new[] { "X-Operation", "X-Resource", "X-Total-Count" };
        public IContainer Container { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddCustomMvc();
            services.AddConsul();
            services.AddJaeger();
            services.AddOpenTracing();


            services.AddEntityFrameworkNpgsql()
                    .AddDbContext<DuaTaxiDbContext>()
                    .BuildServiceProvider();
            services.AddDbContext<DuaTaxiDbContext>(options => options.UseNpgsql(Configuration.GetConnectionString("Default")));

            services.AddIdentity<AppUser, IdentityRole>(options => {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
            }).AddEntityFrameworkStores<DuaTaxiDbContext>()
              .AddDefaultTokenProviders();


            services.AddIdentityServer(opt => {
                opt.Authentication.CookieLifetime = new TimeSpan(365, 0, 0, 0);
                opt.Authentication.CookieSlidingExpiration = true;
                opt.IssuerUri = Configuration["IdentityUrl"];
                opt.PublicOrigin = Configuration["PublicOrigin"];
            }).AddDeveloperSigningCredential()
                // this adds the operational data from DB (codes, tokens, consents)
                .AddOperationalStore(options => {
                    options.ConfigureDbContext = _builder => _builder.UseNpgsql(Configuration.GetConnectionString("Default"));
                    // this enables automatic token cleanup. this is optional.
                    options.EnableTokenCleanup = true;
                    options.TokenCleanupInterval = 30; // interval in seconds
                })
                .AddInMemoryPersistedGrants()
                .AddInMemoryIdentityResources(Config.GetIdentityResources())
                .AddInMemoryApiResources(Config.GetApiResources())
                .AddInMemoryClients(Config.GetClients())
                .AddAspNetIdentity<AppUser>()
                .AddJwtBearerClientAuthentication();

          //  services.AddSession();
            //services.AddAuthentication("Bearer")
            // .AddCookie(options => {
            //     // add an instance of the patched manager to the options:   
            //     // options.Cookie.Name = "idsrv_identity";
            //     options.AccessDeniedPath = "/AccessDenied";
            //     options.SlidingExpiration = true;
            //     options.ExpireTimeSpan = TimeSpan.FromHours(24);
            // }).AddJwtBearer("Bearer", options => {
            //     options.Authority = "http://localhost:5000";
            //     options.RequireHttpsMetadata = false;
            //     options.Audience = "resourceapi";
            //     options.TokenValidationParameters = new TokenValidationParameters() {
            //         ClockSkew = TimeSpan.FromHours(24),
            //         //Na kanw mia dokimh me auto.
            //         //-----RoleClaimType= "role"

            //     };
            // });
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
             .AddCookie("Cookies")
                 //.AddIdentityServerAuthentication(options => {
                 //    options.Authority = "http://localhost:5000";
                 //    options.SupportedTokens = SupportedTokens.Jwt;                 
                 //    options.RequireHttpsMetadata = false; // Note: Set to true in production
                 //    options.ApiName = "resourceapi";                      
            .AddGoogle("Google", options => {                
                options.ClientId = "438908707521-t5bems2ajbps6o8ekbhq9d18ii6v2k3m.apps.googleusercontent.com";
                options.ClientSecret = "46Q79IBlzNkTfms2tWTT0PQL";
               
            })
            .AddFacebook(config => {
                config.AppId = "770513806823886";
                config.AppSecret = "dac3ff637469fe62cfadf8b4fb2f19a3";
               
            })
             //});
             .AddJwtBearer("Bearer", o => {
                 o.Authority = Configuration["IdentityUrl"];
                 o.Audience = "resourceapi";
                 o.RequireHttpsMetadata = false;
                 o.TokenValidationParameters = new TokenValidationParameters() {
                     ClockSkew = TimeSpan.FromDays(1),
                    // Na kanw mia dokimh me auto.

                     //---- - RoleClaimType = "role"

                 };
             });
            //services.ConfigureApplicationCookie(options => {
            //    options.Cookie.Name = "MyCookie1";
            //    options.Cookie.Expiration = TimeSpan.FromDays(24);
            //    options.ExpireTimeSpan = TimeSpan.FromDays(24);
            //    options.SlidingExpiration = true;                  
            //});



            //services.AddAuthentication(options => {
            //    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //}).AddCookie("Cookies", options => {
            //    add an instance of the patched manager to the options:   
            //          options.Cookie.Name = "idsrv_identity";
            //    options.AccessDeniedPath = "/AccessDenied";
            //    options.SlidingExpiration = true;
            //    options.ExpireTimeSpan = TimeSpan.FromHours(24);
            //}).AddJwtBearer("Bearer", o => {
            //    o.Authority = "http://localhost:5000";
            //    o.Audience = "resourceapi";
            //    o.RequireHttpsMetadata = false;
            //    o.TokenValidationParameters = new TokenValidationParameters() {
            //        ClockSkew = TimeSpan.FromMinutes(0),
            //        Na kanw mia dokimh me auto.
            //        ---- - RoleClaimType = "role"

            //    };
            //});


            //services.AddAuthorization(opt => {
            //    opt.AddPolicy("ApiReader", policy => policy.RequireClaim("scope", "api.read"));
            //    opt.AddPolicy("Consumer", policy => policy.RequireClaim(ClaimTypes.Role, "consumer"));
            //});

           
               


            services.AddTransient<IProfileService, IdentityClaimsProfileService>();

            // services.AddCors(options => options.AddPolicy("AllowOrigin", p => p.WithOrigins("https://webeasytravel.firebaseapp.com/")
            //    .AllowAnyMethod()
            //    .AllowAnyHeader()));

            services.AddCors(options => {
                options.AddPolicy("CorsPolicy", cors =>
                        cors.WithOrigins(Configuration["Cors"])
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .AllowCredentials());
                            //.WithExposedHeaders(Headers));
            });


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            // services.AddHttpsRedirection(options =>
            // {
            //     options.HttpsPort = 5550;
            // });
            services.AddScoped<AuthValidation>();
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyTypes(Assembly.GetEntryAssembly())
                    .AsImplementedInterfaces();
            builder.Populate(services);
            builder.AddRabbitMq();
            builder.AddCustomerCheck();
            builder.AddDispatchers();

            Container = builder.Build();

            return new AutofacServiceProvider(Container);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
          IApplicationLifetime applicationLifetime, IConsulClient client,
          IStartupInitializer startupInitializer,
           ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment() || env.EnvironmentName == "local") {
                app.UseDeveloperExceptionPage();
            }
            Console.WriteLine("$EnvironmentName ", env.EnvironmentName);
            app.UseCors("CorsPolicy");
            app.UseExceptionHandler(builder => {
                builder.Run(async context => {
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.Headers.Add("Access-Control-Allow-Origin", "*");

                    var error = context.Features.Get<IExceptionHandlerFeature>();
                    if (error != null) {
                        context.Response.AddApplicationError(error.Error.Message);
                        await context.Response.WriteAsync(error.Error.Message).ConfigureAwait(false);
                    }
                });
            });

            var serilog = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .Enrich.FromLogContext()
                .WriteTo.File(@"authserver_log.txt");

            //loggerFactory.WithFilter(new FilterLoggerSettings
            //    {
            //        { "IdentityServer4", LogLevel.Debug },
            //        { "Microsoft", LogLevel.Warning },
            //        { "System", LogLevel.Warning },
            //    }).AddSerilog(serilog.CreateLogger());

            app.UseStaticFiles();
         
            /// is used for Redirect calls to Https default localhost:5001
            //app.UseHttpsRedirection();
            app.UseIdentityServer();
            app.UseServiceId();
            app.UseErrorHandler();
            app.UseRabbitMq();
            //app.UseSession();            
          
            //app.UseAllForwardedHeaders();
            var forwardedHeaderOptions = new ForwardedHeadersOptions {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedHost | ForwardedHeaders.XForwardedProto
            };
            forwardedHeaderOptions.KnownNetworks.Clear();
            forwardedHeaderOptions.KnownProxies.Clear();
            app.UseForwardedHeaders(forwardedHeaderOptions);

            app.UseAuthentication();
            app.UseRabbitMq()
                    .SubscribeCommand<DeleteUserAccount>()
                    .SubscribeCommand<UpdateUser>()
                    .SubscribeCommand<UpdateUserPassword>();

            var consulServiceId = app.UseConsul();
            applicationLifetime.ApplicationStopped.Register(() => {
                client.Agent.ServiceDeregister(consulServiceId);
                Container.Dispose();
            });

            startupInitializer.InitializeAsync();


            app.UseMvc(routes => {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
