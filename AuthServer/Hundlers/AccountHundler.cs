﻿using AuthServer.Infrastructure.Constants;
using DuaTaxi.AuthServer.Messages.Customers.Delete;
using DuaTaxi.AuthServer.Messages.Customers.Update;
using DuaTaxi.AuthServer.Messages.DeleteAccount;
using DuaTaxi.AuthServer.Messages.UpdatePassword;
using DuaTaxi.AuthServer.Messages.UpdateUserInfo;
using DuaTaxi.Common;
using DuaTaxi.Common.Handlers;
using DuaTaxi.Common.Mvc;
using DuaTaxi.Common.RabbitMq;
using DuaTaxi.Common.Types;
using DuaTaxi.Entities.Core.Models;
using IdentityServer4.Services;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace DuaTaxi.AuthServer.Hundlers
{
    public class AccountHundler : ICommandHandler<DeleteUserAccount>, ICommandHandler<UpdateUser>, ICommandHandler<UpdateUserPassword>
    {

        private readonly ILogger<AccountHundler> _logger;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly UserManager<AppUser> _userManager;
        private readonly IIdentityServerInteractionService _interaction;
        private readonly IAuthenticationSchemeProvider _schemeProvider;
        private readonly IClientStore _clientStore;
        private readonly IEventService _events;
        private readonly IBusPublisher _busPublisher;



        public AccountHundler(UserManager<AppUser> userManager,
                                 SignInManager<AppUser> signInManager,
                                 IBusPublisher busPublisher,
                                 IIdentityServerInteractionService interaction,
                                 IAuthenticationSchemeProvider schemeProvider,
                                 IClientStore clientStore,
                                 IEventService events,
                                 ILogger<AccountHundler> logger)

        {
            _userManager = userManager;
            _interaction = interaction;
            _schemeProvider = schemeProvider;
            _clientStore = clientStore;
            _events = events;
            _busPublisher = busPublisher;
            _signInManager = signInManager;
            _logger = logger;
        }

        public async Task HandleAsync(DeleteUserAccount command, ICorrelationContext context)
        {
            try {
                var user = await _userManager.FindByEmailAsync(command.Email);

                if (user is null) {
                    throw new DuaTaxiException($"User with email : {command.Email} does not exist ");
                }
                var getclaims = await _userManager.GetClaimsAsync(user);
                var role = getclaims.Where(x => x.Type.Contains("role")).Select(z => z.Value).FirstOrDefault();

                switch (role) {
                    case Roles.TaxiDriver:
                    var deletedriver = new DeleteTaxiDriverCustomer(Guid.Empty.ToString(), user.Id);
                    await _busPublisher.SendAsync(deletedriver, context);
                    break;
                    case Roles.MiniBusDriver:
                    var deletedriver2 = new DeleteMiniBusDriverCustomer(Guid.Empty.ToString(), user.Id);
                    await _busPublisher.SendAsync(deletedriver2, context);
                    break;
                    case Roles.BusDriver:
                    var deletedriver3 = new DeleteBusDriverCustomer(Guid.Empty.ToString(), user.Id);
                    await _busPublisher.SendAsync(deletedriver3, context);
                    break;
                    default:
                    break;
                }

                await _userManager.DeleteAsync(user);

                await _busPublisher.PublishAsync(new DeleteAccountCompleted(
                                    command.Id,
                                    $"User with id: '{command.Id}' Deleted Successfully",
                                    "SUCCESS"),
                                    context);
                return;

            } catch (Exception ex) {

                _logger.LogError($"Customer could not Created with error {ex.StackTrace}");
                await _busPublisher.PublishAsync(new DeleteAccountRejected(
                   command.Id,
                   $"Customer with id: '{command.Id}' could not Created with error {ex.StackTrace}",
                   "Create TaxiDriverCustomer Exception"),
                   context);
                return;
            }


        }

        public async Task HandleAsync(UpdateUser command, ICorrelationContext context)
        {
            try {

                if (command == null)
                    throw new DuaTaxiException($"User with email :{command.Email} does not exist ");

                AppUser appUser = await _userManager.FindByIdAsync(command.Id);

                var getclaims = await _userManager.GetClaimsAsync(appUser);
                var role = getclaims.Where(x => x.Type.Contains("role")).Select(z => z.Value).FirstOrDefault();

                if (appUser == null)
                    throw new DuaTaxiException($"User Not Found");


                appUser.CarPlate = command.CarPlate;
                appUser.Email = command.Email;
                appUser.UserName = command.Email;
                appUser.Name = command.Name;
                appUser.PhoneNumber = command.PhoneNumber;

                var result = await _userManager.UpdateAsync(appUser);
                AppUser afterUpdateUser = await _userManager.FindByIdAsync(command.Id);
                if (result.Succeeded) {


                    switch (role) {

                        case Roles.TaxiDriver:
                        var createTaxiDriver = new UpdateTaxiDriverCustomer(Guid.NewGuid().ToString(), afterUpdateUser.Id, afterUpdateUser.Name, afterUpdateUser.Email, afterUpdateUser.PhoneNumber, DriverTypes.TaxiDriver, afterUpdateUser.CarPlate);
                        await _busPublisher.SendAsync(createTaxiDriver, CorrelationContext.Empty);
                        break;
                        case Roles.MiniBusDriver:
                        var createMiniBusDriver = new UpdateMiniBusDriverCustomer(afterUpdateUser.Id, afterUpdateUser.Name, afterUpdateUser.Email, afterUpdateUser.PhoneNumber, DriverTypes.TaxiDriver, afterUpdateUser.CarPlate);
                        await _busPublisher.SendAsync(createMiniBusDriver, context);
                        break;
                        case Roles.BusDriver:
                        var createBusDriver = new UpdateBusDriverCustomer(afterUpdateUser.Id, afterUpdateUser.Name, afterUpdateUser.Email, afterUpdateUser.PhoneNumber, DriverTypes.TaxiDriver, afterUpdateUser.CarPlate);
                        await _busPublisher.SendAsync(createBusDriver, context);
                        break;
                        default:
                        break;
                    }

                }

            } catch (Exception ex) {

                _logger.LogError($"Customer could not Created with error {ex.StackTrace}");
                await _busPublisher.PublishAsync(new UpdateUserRejected(
                   command.Id,
                   $"User with id: '{command.Id}' could not updated with error {ex.StackTrace}",
                   "ERROR"),
                   context);
                return;
            }

            await _busPublisher.PublishAsync(new UpdateUserCompleted(
               command.Id,
               $"User with id: '{command.Id}' updated Successfully",
               "SUCCESS"),
               context);
            return;


        }

        public async Task HandleAsync(UpdateUserPassword command, ICorrelationContext context)
        {
            try {

                if (command == null)
                    throw new DuaTaxiException($"Invalid model");


                AppUser appUser = await _userManager.FindByIdAsync(command.Id);
                if (appUser == null)
                    throw new DuaTaxiException($"User with email :{command.Email} does not exist ");

                bool isPasswordChanged = !string.IsNullOrWhiteSpace(command.NewPassword);

                if (string.IsNullOrWhiteSpace(command.Password)) {
                    if (isPasswordChanged)
                        throw new DuaTaxiException($"Current password is required when changing your own password");

                }
                else if (isPasswordChanged) {
                    if (!await _userManager.CheckPasswordAsync(appUser, command.Password))
                        throw new DuaTaxiException($"The username/password couple is invalid.");
                    else
                        await ResetPasswordAsync(appUser, command.NewPassword);
                }

                var result = await _userManager.UpdateAsync(appUser);

                if (result.Succeeded) {
                    await _busPublisher.PublishAsync(new UpdateUserPasswordCompeted(
                  command.Id,
                  $"Password is updated successfully",
                  "SUCCESS"),
                  context);
                    return;
                }


            } catch (DuaTaxiException ex) {

                _logger.LogError($"Customer could not Created with error {ex.StackTrace}");
                await _busPublisher.PublishAsync(new UpdateUserPasswordRejected(
                   command.Id,
                   $"{ex.Code}",
                   "ERROR"),
                   context);
                return;
            }


        }
        public async Task<(bool Succeeded, string[] Errors)> ResetPasswordAsync(AppUser user, string newPassword)
        {
            string resetToken = await _userManager.GeneratePasswordResetTokenAsync(user);

            var result = await _userManager.ResetPasswordAsync(user, resetToken, newPassword);
            if (!result.Succeeded)
                return (false, result.Errors.Select(e => e.Description).ToArray());

            return (true, new string[] { });
        }

    }
}
