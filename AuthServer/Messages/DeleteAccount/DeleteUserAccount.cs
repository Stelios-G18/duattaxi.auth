﻿using DuaTaxi.Common.Messages;
using DuaTaxi.Common.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.AuthServer.Messages.DeleteAccount
{
    public class DeleteUserAccount : IIdentifiable, ICommand
    {
        public string Id { get; set; }
        public string Email { get; set; }
    }
}
