﻿using DuaTaxi.Common.Messages;
using DuaTaxi.Common.Types;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.AuthServer.Messages.Customers.Update
{
    [MessageNamespace("busapi")]
    public class UpdateBusDriverCustomer : IIdentifiable, ICommand
    {

        public string Id { get; set; }
        public string CustomerId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }


        public string PhoneNumber { get; set; }

        public DriverTypes Type { get; set; }

        public string CarPlate { get; set; }


        [JsonConstructor]
        public UpdateBusDriverCustomer( string CustomerId, string Name, string Email, string PhoneNumber, DriverTypes types,string CarPlate)
        {
            this.CustomerId = CustomerId;
            this.Name = Name;
            this.Email = Email;
            this.PhoneNumber = PhoneNumber;
            this.Type = types;
            this.CarPlate = CarPlate;
        }
    }
}
