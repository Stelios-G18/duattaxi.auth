﻿using DuaTaxi.Common.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.AuthServer.Messages.UpdateUserInfo
{
    public class UpdateUserRejected : IRejectedEvent
    {
        public string Id { get; protected set; }
        public string Reason { get; protected set; }
        public string Code { get; protected set; }
       
        [JsonConstructor]
        public UpdateUserRejected(string Id, string reason, string code)
        {
            this.Id = Id;
            this.Reason = reason;
            this.Code = code;
        }
    }
}
