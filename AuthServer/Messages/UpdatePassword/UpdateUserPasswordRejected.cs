﻿using DuaTaxi.Common.Messages;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.AuthServer.Messages.UpdatePassword
{
    public class UpdateUserPasswordRejected   : IRejectedEvent
    {
        public string Id { get; protected set; }
        public string Reason { get; protected set; }
        public string Code { get; protected set; }

        [JsonConstructor]
        public UpdateUserPasswordRejected(string Id, string reason, string code)
        {
            this.Id = Id;
            this.Reason = reason;
            this.Code = code;
        }
    }
}
