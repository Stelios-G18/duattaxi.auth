﻿using DuaTaxi.Common.Messages;
using DuaTaxi.Common.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.AuthServer.Messages.UpdatePassword
{
    public class UpdateUserPassword : IIdentifiable, ICommand
    {
        public string Id { get; set; }


        public string Email { get; set; }


        public string Password { get; set; }


        public string NewPassword { get; set; }


        public string ConfigPassword { get; set; }
    }
}



